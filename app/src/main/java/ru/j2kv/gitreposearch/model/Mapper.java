package ru.j2kv.gitreposearch.model;

import java.util.ArrayList;
import java.util.List;

import ru.j2kv.gitreposearch.model.Pojo.Item;

public class Mapper {
    public static List<GHRepository> map(List<Item> resultItems){
        List<GHRepository> ghRepositories = new ArrayList<>();
        for (Item item:resultItems) {
            ghRepositories.add(
                    new GHRepository(
                            item.getOwner().getAvatarUrl(),
                            item.getFullName(),
                            item.getDescription()));
        }
        return ghRepositories;
    }
}
