package ru.j2kv.gitreposearch.model;

public class GHRepository {
    private String avatarUrl;
    private String name;

    public GHRepository(String avatarUrl, String name, String description) {
        this.avatarUrl = avatarUrl;
        this.name = name;
        this.description = description;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String description;
}
