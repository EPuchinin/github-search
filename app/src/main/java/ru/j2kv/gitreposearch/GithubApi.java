package ru.j2kv.gitreposearch;

import io.reactivex.Flowable;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import ru.j2kv.gitreposearch.model.Pojo.SearchResult;
import ru.j2kv.gitreposearch.model.Pojo.User;

public interface GithubApi {
    @GET("/search/repositories")
    @Headers("Content-Type:application/json")
    Flowable<SearchResult> getRepositoriesByNameAuth(@Header("Authorization") String base64string, @Query("q") String repoName, @Query("page") int requestPage, @Query("per_page") int itemsPerPage);

    @GET("/search/repositories")
    @Headers("Content-Type:application/json")
    Flowable<SearchResult> getRepositoriesByName(@Query("q") String repoName, @Query("page") int requestPage, @Query("per_page") int itemsPerPage);


    @GET("/user")
    @Headers("Content-Type:application/json")
    Single<User> authenticate(@Header("Authorization") String base64string);

}
