package ru.j2kv.gitreposearch.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.j2kv.gitreposearch.reposotory.Repository;
import ru.j2kv.gitreposearch.reposotory.RepositoryImpl;

@Module (includes = ApiModule.class)
public class RepositoryModule {
    @Singleton
    @Provides
    Repository provideRepository(){
        return new RepositoryImpl();
    }
}
