package ru.j2kv.gitreposearch.di;

import javax.inject.Singleton;

import dagger.Component;
import ru.j2kv.gitreposearch.login.LoginPresenter;
import ru.j2kv.gitreposearch.reposotory.RepositoryImpl;
import ru.j2kv.gitreposearch.search.SearchPresenter;

@Singleton
@Component(modules = {ContextModule.class, PresenterModule.class, RepositoryModule.class})
public interface AppComponent {
    void injectSearchPresenter(SearchPresenter searchPresenter);

    void injectLoginPresenter(LoginPresenter loginPresenter);

    void injectRepository(RepositoryImpl repository);
}
