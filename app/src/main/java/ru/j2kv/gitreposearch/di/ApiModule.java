package ru.j2kv.gitreposearch.di;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.j2kv.gitreposearch.GithubApi;

import static ru.j2kv.gitreposearch.Const.GH_URL;

@Module
public class ApiModule {
    @Singleton
    @Provides
    GithubApi provideGithubAPI(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(GH_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build().create(GithubApi.class);
    }

    @Singleton
    @Provides
    OkHttpClient provideOkHttpClient () {
        return new OkHttpClient.Builder()
                .readTimeout(5, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .build();
    }
}
