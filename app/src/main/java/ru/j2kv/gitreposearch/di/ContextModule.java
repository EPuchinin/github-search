package ru.j2kv.gitreposearch.di;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {
    @Provides
    Context provideAppContext(){
        return App.getAppContext();
    }
}
