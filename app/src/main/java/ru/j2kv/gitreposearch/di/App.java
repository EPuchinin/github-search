package ru.j2kv.gitreposearch.di;

import android.app.Application;
import android.content.Context;

public class App extends Application {
    private static AppComponent component;
    private static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerAppComponent.create();
        appContext = this.getApplicationContext();
    }

    public static AppComponent getComponent(){
        return component;
    }

    public static Context getAppContext(){
        return appContext;
    }
}
