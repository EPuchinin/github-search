package ru.j2kv.gitreposearch.di;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.j2kv.gitreposearch.login.ILoginPresenter;
import ru.j2kv.gitreposearch.login.LoginPresenter;
import ru.j2kv.gitreposearch.search.ISearchPresenter;
import ru.j2kv.gitreposearch.search.SearchPresenter;


@Module
public class PresenterModule {

    @Singleton
    @Provides
    ILoginPresenter provideLoginPresenter(){return new LoginPresenter();
    }

    @Singleton
    @Provides
    ISearchPresenter provideSearchPresenter(){return new SearchPresenter();
    }
}
