package ru.j2kv.gitreposearch.login;

public interface ILoginPresenter {
    void login(String email, String password);
}
