package ru.j2kv.gitreposearch.login;

import com.arellomobile.mvp.MvpView;

public interface ILoginView extends MvpView {
    void forwardToSearch(String authString);

    void showLoading(boolean show);

    void showError(String message);

}
