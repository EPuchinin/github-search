package ru.j2kv.gitreposearch.login;

import android.util.Base64;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import ru.j2kv.gitreposearch.di.App;
import ru.j2kv.gitreposearch.reposotory.Repository;

@InjectViewState
public class LoginPresenter extends MvpPresenter<ILoginView> implements ILoginPresenter {
    @Inject
    Repository repository;

    private ILoginView view;

    public LoginPresenter() {
        App.getComponent().injectLoginPresenter(this);
        view = getViewState();
    }

    @Override
    public void login(String email, String password) {
        String loginEncodedString = Base64.encodeToString((email + ":" + password).getBytes(), Base64.NO_WRAP);
        view.showLoading(true);
        repository.login(loginEncodedString)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> view.showLoading(false))
                .subscribe(user -> view.forwardToSearch(loginEncodedString),
                        this::handleError);
    }


    private void handleError(Throwable e) {
        if (e instanceof HttpException) {
//            ResponseBody responseBody = ((HttpException) e).response().errorBody();
//            int response = ((HttpException) e).response().code();
            view.showError("Authentication failed");
        } else if (e instanceof SocketTimeoutException) {
            view.showError("Connection Error");
        } else if (e instanceof UnknownHostException) {
            view.showError(e.getLocalizedMessage());
        } else if (e instanceof IOException) {
            view.showError("Network error");
        } else {
            view.showError("UnknownError " + e.getMessage());
        }
    }
}
