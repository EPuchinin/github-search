package ru.j2kv.gitreposearch.login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.j2kv.gitreposearch.R;
import ru.j2kv.gitreposearch.search.SearchActivity;

import static ru.j2kv.gitreposearch.Const.AUTH_RESULT;


public class LoginActivity extends MvpAppCompatActivity implements ILoginView {
    @BindView(R.id.login_email)
    EditText etEmail;
    @BindView(R.id.login_password)
    EditText etPassword;
    @BindView(R.id.login_progress)
    View pb_login_loading;
    @BindView(R.id.login_form)
    View loginFormView;
    @BindView(R.id.iv_logo)
    ImageView ivLogo;
    @BindView(R.id.tv_pass_sign_in)
    TextView tvPassSignin;
    @BindView(R.id.btn_login_sign_in)
    Button btnSignin;
    @InjectPresenter
    LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        tvPassSignin.setOnClickListener(v -> forwardToSearch(null));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            ivLogo.setImageDrawable(getDrawable(R.drawable.ic_logo));
        etPassword.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin();
                return true;
            }
            return false;
        });
        btnSignin.setOnClickListener(view -> attemptLogin());
    }

    @Override
    public void forwardToSearch(String authString) {
        Intent intent = new Intent(this, SearchActivity.class);
        intent.putExtra(AUTH_RESULT, authString);
        startActivity(intent);
        finish();
    }


    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showLoading(boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        loginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        pb_login_loading.setVisibility(show ? View.VISIBLE : View.GONE);
        pb_login_loading.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                pb_login_loading.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void showError(String localizedMessage) {
        Toast.makeText(this, localizedMessage, Toast.LENGTH_LONG).show();
    }

    private void hideKeyboard() {
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(Objects.requireNonNull(
                        this.getCurrentFocus()).getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    private void attemptLogin() {
        etEmail.setError(null);
        etPassword.setError(null);
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            etPassword.setError(getString(R.string.error_invalid_password));
            focusView = etPassword;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            etEmail.setError(getString(R.string.error_field_required));
            focusView = etEmail;
            cancel = true;
        } else if (!isEmailValid(email)) {
            etEmail.setError(getString(R.string.error_invalid_email));
            focusView = etEmail;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            hideKeyboard();
            presenter.login(email, password);
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@") && email.length() > 6;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }
}

