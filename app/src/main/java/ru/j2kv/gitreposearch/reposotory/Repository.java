package ru.j2kv.gitreposearch.reposotory;

import io.reactivex.Flowable;
import io.reactivex.Single;
import ru.j2kv.gitreposearch.model.Pojo.SearchResult;
import ru.j2kv.gitreposearch.model.Pojo.User;

public interface Repository {
//    Flowable<SearchResult> searchRepoByName(String authString,String repoName, int page);
    Flowable<SearchResult> searchRepoByName(String authString,String repoName, int pageNumber, int itemsPerPage);
    Single<User> login(String loginEncodedString);
}


