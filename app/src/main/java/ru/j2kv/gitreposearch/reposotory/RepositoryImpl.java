package ru.j2kv.gitreposearch.reposotory;

import android.util.Log;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Single;
import ru.j2kv.gitreposearch.GithubApi;
import ru.j2kv.gitreposearch.di.App;
import ru.j2kv.gitreposearch.model.Pojo.SearchResult;
import ru.j2kv.gitreposearch.model.Pojo.User;

import static ru.j2kv.gitreposearch.Const.TAG;

public class RepositoryImpl implements Repository {
    @Inject
    GithubApi githubApi;


    public RepositoryImpl() {
        App.getComponent().injectRepository(this);
    }

    @Override
    public Flowable<SearchResult> searchRepoByName(String authString,String repoName, int pageNumber, int itemsPerPage) {
        Log.e(TAG, "searchRepoByName: authString: "+ authString + ", repoName:" + repoName + ", pageNumber: " + pageNumber + ", itemsPerPage: " + itemsPerPage);
        if (authString != null)
            return githubApi.getRepositoriesByNameAuth(authString, repoName, pageNumber, itemsPerPage)
                    .doOnError(throwable -> Log.e(TAG, "RepositoryImpl: githubApi.getRepositoriesByNameAuth: ", throwable));
        else return githubApi.getRepositoriesByName(repoName, pageNumber,itemsPerPage )
                .doOnError(throwable -> Log.e(TAG, "RepositoryImpl: githubApi.getRepositoriesByName: ", throwable));
    }

    @Override
    public Single<User> login(String loginEncodedString) {
        return githubApi.authenticate("Basic " + loginEncodedString);
    }
}
