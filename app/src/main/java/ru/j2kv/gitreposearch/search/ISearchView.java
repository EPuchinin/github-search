package ru.j2kv.gitreposearch.search;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import java.util.List;

import io.reactivex.Observable;
import ru.j2kv.gitreposearch.model.GHRepository;

//https://github.com/Arello-Mobile/Moxy/wiki/View-commands-state-strategy#existing-strategies
public interface ISearchView extends MvpView {
    @StateStrategyType(SkipStrategy.class)
    void showLoading(boolean show);
    void showError(String message);
    void showMessage(String message);
    @StateStrategyType(AddToEndSingleStrategy.class)
    void setData(SearchPageListAdapter adapter);
}
