package ru.j2kv.gitreposearch.search;

import android.arch.paging.PagedListAdapter;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.j2kv.gitreposearch.R;
import ru.j2kv.gitreposearch.model.GHRepository;

import static ru.j2kv.gitreposearch.Const.TAG;

public class SearchPageListAdapter extends PagedListAdapter<GHRepository, GHRepositoryViewHolder> {
    SearchPageListAdapter(DiffUtil.ItemCallback<GHRepository> diffUItemCallback) {
        super(diffUItemCallback);
    }

    @NonNull
    @Override
    public GHRepositoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item, parent, false);
        return new GHRepositoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GHRepositoryViewHolder holder, int position) {
        GHRepository ghRepository = getItem(position);
        if(ghRepository!=null) {
            holder.repoName.setText(ghRepository.getName());
            holder.repoDesc.setText(ghRepository.getDescription());
            Picasso.get()
                    .load(ghRepository.getAvatarUrl())
                    .placeholder(R.drawable.ic_loading)
                    .error(R.drawable.ic_empty_user)
                    .fit()
                    .centerCrop()
                    .into(holder.avatar, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            Log.e(TAG, "onQueryError: " + e.getLocalizedMessage());
                        }
                    });
        }else{
            holder.repoName.setText(R.string.loading_data);
            holder.repoDesc.setText(R.string.loading_data_desc);
            holder.avatar.setImageResource(R.drawable.ic_empty_user);
        }

    }
}

class GHRepositoryViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.iv_avatar)
    ImageView avatar;
    @BindView(R.id.tv_repo_name)
    TextView repoName;
    @BindView(R.id.tv_repo_desc)
    TextView repoDesc;

    GHRepositoryViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
