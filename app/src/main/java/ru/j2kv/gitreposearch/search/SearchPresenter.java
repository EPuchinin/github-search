package ru.j2kv.gitreposearch.search;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.paging.PagedList;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.util.DiffUtil;
import android.text.TextUtils;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.HttpException;
import ru.j2kv.gitreposearch.di.App;
import ru.j2kv.gitreposearch.model.GHRepository;
import ru.j2kv.gitreposearch.reposotory.Repository;

import static ru.j2kv.gitreposearch.Const.TAG;

@InjectViewState
public class SearchPresenter
        extends MvpPresenter<ISearchView>
        implements ISearchPresenter,
        SearchPositionalDataSource.DataSourceCallback,
        LifecycleObserver {

    @Inject
    Repository repository;
    private String authString = null;
    private ISearchView view;
    private SearchPageListAdapter rvAdapter;
    private PagedList.Config config;
    private SearchPositionalDataSource dataSource;
    private CompositeDisposable disposable = new CompositeDisposable();

    public SearchPresenter() {
        App.getComponent().injectSearchPresenter(this);
        view = getViewState();
        dataSource = new SearchPositionalDataSource(repository, this);
        config = new PagedList.Config.Builder()
                .setEnablePlaceholders(true)
                .setInitialLoadSizeHint(30)
                .setPageSize(30)
                .setPrefetchDistance(10)
                .build();
        rvAdapter = new SearchPageListAdapter(new DiffUtil.ItemCallback<GHRepository>() {
            @Override
            public boolean areItemsTheSame(GHRepository oldItem, GHRepository newItem) {
                return oldItem.equals(newItem);
            }

            @Override
            public boolean areContentsTheSame(GHRepository oldItem, GHRepository newItem) {
                return (oldItem.getName().equals(newItem.getName()) &&
                        oldItem.getDescription().equals(newItem.getDescription()) &&
                        oldItem.getAvatarUrl().equals(newItem.getAvatarUrl()));
            }
        });
        view.setData(rvAdapter);
    }

    @Override
    public void setQueryObsevervable(io.reactivex.Observable<String> query) {
        disposable.add(query
                .debounce(500, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .filter(s -> !TextUtils.isEmpty(s))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> newSearch(authString, s)));
    }

    @Override
    public void newSearch(String authString, String repoName) {
        dataSource.setAuthString(authString);
        dataSource.setQuery(repoName);
        PagedList<GHRepository> pagedList = new PagedList.Builder<>(dataSource, config)
                .setFetchExecutor(Executors.newSingleThreadExecutor())
                .setNotifyExecutor(new MainThreadExecutor())
                .build();
        rvAdapter.submitList(pagedList);
    }

    @Override
    public void setAuth(String authString) {
        this.authString = authString;
    }

    @Override
    public void onQueryStart() {
        view.showLoading(true);
    }

    @Override
    public void onQueryComplete() {
        view.showLoading(false);
    }

    @Override
    public void onQueryError(Throwable e) {
        if (e instanceof HttpException) {
            switch (((HttpException) e).code()) {
                case 403:
                    view.showError("Too much queries/" + e.getLocalizedMessage());
                    return;
                default:
                    view.showError("UnknownError/" + e.getLocalizedMessage());
            }
        } else if (e instanceof SocketTimeoutException) {
            view.showError("Connection Error/" + e.getLocalizedMessage());
        } else if (e instanceof UnknownHostException) {
            view.showError(e.getLocalizedMessage());
        } else if (e instanceof IOException) {
            view.showError("Network error/" + e.getLocalizedMessage());
        } else {
            view.showError("UnknownError/" + e.getLocalizedMessage());
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy() {
        disposable.dispose();
    }


    private static class MainThreadExecutor implements Executor {
        private Handler mainHandler = new Handler(Looper.getMainLooper());

        @Override
        public void execute(Runnable command) {
            mainHandler.post(command);
        }
    }
}
