package ru.j2kv.gitreposearch.search;

import android.arch.paging.PositionalDataSource;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.j2kv.gitreposearch.model.GHRepository;
import ru.j2kv.gitreposearch.model.Mapper;
import ru.j2kv.gitreposearch.reposotory.Repository;

import static ru.j2kv.gitreposearch.Const.TAG;

public class SearchPositionalDataSource extends PositionalDataSource<GHRepository> {
    private final Repository repository;
    private String authString;
    private String query;
    private int totalItems;
    private DataSourceCallback dataSourceCallback;

    public SearchPositionalDataSource(Repository repository, DataSourceCallback dataSourceCallback) {
        this.repository = repository;
        this.dataSourceCallback = dataSourceCallback;
    }


    public void setAuthString(String authString) {
        this.authString = authString;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback<GHRepository> callback) {
        repository.searchRepoByName(authString, query, 1, params.pageSize)
                .doOnNext(searchResult -> totalItems = searchResult.getTotalCount())
                .flatMapIterable(searchResult -> Mapper.map(searchResult.getItems()))
                .toList()
                .toFlowable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(subscription -> dataSourceCallback.onQueryStart())
                .subscribe(ghRepositories -> {
                    dataSourceCallback.onQueryComplete();
                    Log.e(TAG, "SearchPositionalDataSource: ghRepositories.size: " + ghRepositories.size());
                    if (params.placeholdersEnabled)
                        callback.onResult(ghRepositories, 0, totalItems);
                    else callback.onResult(ghRepositories, 0);
                }, throwable -> dataSourceCallback.onQueryError(throwable));


    }

    @Override
    public void loadRange(@NonNull LoadRangeParams params, @NonNull LoadRangeCallback<GHRepository> callback) {
        repository.searchRepoByName(authString, query, (params.startPosition / params.loadSize) + 1, params.loadSize)
                .flatMapIterable(searchResult -> Mapper.map(searchResult.getItems()))
                .toList()
                .toFlowable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
//                .doOnSubscribe(subscription -> dataSourceCallback.onQueryStart())
                .subscribe(ghRepositories -> {
                    callback.onResult(ghRepositories);
                    Log.e(TAG, "SearchPositionalDataSource: params.startPosition: " + params.startPosition + ", params.loadSize: " + params.loadSize);
                    Log.e(TAG, "SearchPositionalDataSource: ghRepositories.size: " + ghRepositories.size());
//                    dataSourceCallback.onQueryComplete();
                }, throwable -> {
//                    dataSourceCallback.onQueryError(throwable);
                    callback.onResult(new ArrayList<>());
                });

    }

    public interface DataSourceCallback {
        void onQueryError(Throwable throwable);

        void onQueryStart();

        void onQueryComplete();
    }
}
