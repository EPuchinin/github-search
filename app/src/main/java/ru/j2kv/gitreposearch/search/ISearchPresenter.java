package ru.j2kv.gitreposearch.search;


import io.reactivex.Observable;

public interface ISearchPresenter {
    void newSearch(String authString, String repoName);
    void setQueryObsevervable(Observable<String> query);
    void setAuth(String authString);
}
