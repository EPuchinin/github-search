package ru.j2kv.gitreposearch.search;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.jakewharton.rxbinding2.widget.RxSearchView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.j2kv.gitreposearch.R;
import ru.j2kv.gitreposearch.login.LoginActivity;

import static ru.j2kv.gitreposearch.Const.AUTH_RESULT;

public class SearchActivity extends MvpAppCompatActivity implements ISearchView {

    @InjectPresenter
    SearchPresenter presenter;
    @BindView(R.id.rv_search_results)
    RecyclerView rvSearchResults;
    @BindView(R.id.pb_search_loading)
    ProgressBar pbSearchLoading;
    @BindView(R.id.tv_search_message)
    TextView tvMessage;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private SearchView searchView;
    private String authString = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        authString = getIntent().getStringExtra(AUTH_RESULT);
        presenter.setAuth(authString);
        initViews();
        getLifecycle().addObserver(presenter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sign_in:
                forwardToLogin();
                return true;
            case R.id.sign_out:
                authString = null;
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void forwardToLogin() {
        startActivity(new Intent(this, LoginActivity.class));
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.sign_in).setVisible(authString == null);
        menu.findItem(R.id.sign_out).setVisible(authString != null);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        presenter.setQueryObsevervable(RxSearchView.queryTextChanges(searchView)
                .map(CharSequence::toString));
        searchView.requestFocus();
        return true;
    }

    private void initViews() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvSearchResults.setLayoutManager(linearLayoutManager);
        rvSearchResults.setItemAnimator(new DefaultItemAnimator());
        rvSearchResults.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }


    void hideKeyboard() {
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void showLoading(boolean show) {
        if (show) {
            hideKeyboard();
            pbSearchLoading.setVisibility(View.VISIBLE);
            tvMessage.setVisibility(View.GONE);
        } else pbSearchLoading.setVisibility(View.GONE);


    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(String message) {
        tvMessage.setVisibility(View.VISIBLE);
        tvMessage.setText(message);
    }

    @Override
    public void setData(SearchPageListAdapter adapter) {
        rvSearchResults.setAdapter(adapter);
    }
}
