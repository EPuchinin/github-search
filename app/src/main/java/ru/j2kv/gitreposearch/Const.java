package ru.j2kv.gitreposearch;

public class Const {
    public static final String GH_URL = "https://api.github.com/";
    public static final String TAG = "TAG";
    public static final String AUTH_RESULT = "AuthResult";
}
